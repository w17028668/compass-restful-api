<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User; 

class Review extends Model
{
    
    protected $fillable = ['user_id', 'place_id', 'review_text', 'rating', 'thumbs_up', 'thumbs_down','name', 'price'];

    //relationship beetwen review and user
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
