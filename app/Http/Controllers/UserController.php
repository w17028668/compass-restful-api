<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request; 
use App\Http\Controllers\Controller; 
use App\User; 
use Illuminate\Support\Facades\Auth; 
use Validator;
use App\Notifications\SignupActivate;
use Carbon;
use Hash;

class UserController extends Controller 
{
    /** 
     * login api 
     * 
     * @return \Illuminate\Http\Response 
     */ 

    public $successStatus = 200;

    public function login(Request $request){ 

        //validation for login 
        $request->validate([
            'email' => 'required|string|email',
            'password' => 'required|string',
        ]);

        $credentials = request(['email', 'password']);
        $credentials['active'] = 1;
        $credentials['deleted_at'] = null;
        $wrongUser = User::where('email', request(['email']))->first();

        if($wrongUser === null){
            return response()->json([
                'errors' => [
                    'unauthorized' =>['This user does not exist'],
                    
                ]
            ], 401);
            
        }else if($wrongUser->active === 0){

            return response()->json([
                'errors' => [
                    'unauthorized' =>['Please confirm your account'],
                    
                ]
            ], 401);

        }else if(!Auth::attempt($credentials)){
            return response()->json([
                'errors' => [
                    'unauthorized' =>['Incorrect email or password.'],
                    
                ]
            ], 401);
        }
        $user = $request->user();
        $tokenResult = $user->createToken('Personal Access Token');
        $token = $tokenResult->token;
        if ($request->remember_me)
            $token->expires_at = Carbon::now()->addWeeks(1);
        $token->save();
        $success['token'] =  $tokenResult->accessToken;
        $success['token_type'] = 'Bearer';
        $success['expires_at'] = Carbon::parse($tokenResult->token->expires_at)->toDateTimeString();    
        return response()->json(['success' => $success], $this-> successStatus); 
    }
    /** 
     * Register api 
     * 
     * @return \Illuminate\Http\Response 
     */ 
    public function register(Request $request) 
    {   
       
        $request->validate([
            'name' => 'required', 
            'email' => 'required|email', 
            'password' => 'required|min:6', 
            'c_password' => 'required|same:password', 
            
        ],[
            'c_password.same' => 'The password and the confirmation must match.',
            'c_password.required' => 'The password confirmation field is required.'
        ]);
       
        $wrongUser = User::where('email', request(['email']))->first();
        if($wrongUser !== null){
            return response()->json([
                'errors' => [
                    'unauthorized' =>['User already exists.'],
                    
                ]
            ], 401);
        }else{   
        $input = $request->all(); 
                $input['password'] = bcrypt($input['password']); 
                $input['activation_token'] = str_random(60);
                $user = User::create($input);
                $user->notify(new SignupActivate($user)); 
                $success['token'] =  $user->createToken('MyApp')-> accessToken; 
                $success['name'] =  $user->name;
                $success['message'] = 'Successfully created user!';
                return response()->json(['success'=>$success], $this-> successStatus); 
            }

    }        
    //activates account
    public function signupActivate($token)
    {
        $user = User::where('activation_token', $token)->first();
        if (!$user) {
            return view("confirmationPageError");
        }
        $user->active = true;
        $user->activation_token = '';
        $user->save();
        return view("confirmationPage");
    }        
    
    public function details() 
    { 
        $user = Auth::user(); 
        $reviews = $user->reviews;
        $success['details'] =  $user;
        $success['reviews'] =  $reviews;
        return response()->json(['success' => $success], $this-> successStatus); 
    }
    
    public function delete() 
    { 
        $user = Auth::user();
        $reviews = $user->reviews; 
        foreach($reviews as $review){
            $review->delete();
        }
        $user->delete();
        $success = 'Your account has been deleted.' ;
        return response()->json(['success' => $success], $this-> successStatus); 
    } 

    //to change pwd
    public function update(Request $request) 
    { 
        $request->validate([
            'password' => 'required|min:6', 
            'c_password' => 'required|same:password', 
            
        ],[
            'c_password.same' => 'The password and the confirmation must match.',
            'c_password.required' => 'The password confirmation is required'
        ]);

        $user = Auth::user();
        $newPassword = Hash::make($request->password);
        $user->password =  $newPassword;
        $user->save(); 

        $success = 'Your password has been changed' ;

        return response()->json(['success' => $success], $this-> successStatus); 
    } 

    public function setPreferences(Request $request) 
    { 
        $request->validate([
            'radius' => 'required|numeric', 
            
        ]);

        $user = Auth::user();
        $user->radius =  $request['radius'];
        $user->preference =  $request['preference'];
        $user->save(); 

        $success = 'Your preferences have been set' ;

        return response()->json(['success' => $success], $this-> successStatus); 
    } 
    //deletes authAcessToken in order to logout user
    public function logoutApi()
    { 
        if (Auth::check()) {
           Auth::user()->AauthAcessToken()->delete();
        }

        $success = 'Logged out' ;

        return response()->json(['success' => $success], $this-> successStatus); 
    }
    
}