<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth; 
use App\User;
use App\Review;  
use Validator;
use Log;

class ReviewController extends Controller
{   
    //to send back sucess response
    public $successStatus = 200;

    function getReviews(Request $request){

        $reviews= Review::where('place_id', $request->input('place_id'))
        ->orderBy('created_at', 'desc')
        ->with('user')->get();
        return response()->json(['success'=>$reviews], $this-> successStatus); 

    }

    function publish(Request $request){

        $user = Auth::user();

        //review validation 
        $request->validate([
            'place_id' => 'required', 
            'review_text' => 'required', 
            'rating' => 'required', 
            'price' => 'sometimes|numeric'
    
        ]);
       
        $request->request->add(['user_id' => $user->id]);
        $input = $request->all(); 
                $review = Review::create($input); 
                $success = 'Your review has been published' ;
        return response()->json(['success'=>$success], $this-> successStatus); //sends back success message
        

    }

    //adds three new properties to JSON object
    function getAverage(Request $request){

        
        if($request['categoryID'] === 0){
     
            $new[] = collect($request['placesArray'])->map(function ($place)  {

                if(Review::where('place_id', $place['place_id'])){

                    $place['newRating'] = number_format((float)Review::where('place_id', $place['place_id'])->pluck('rating')->avg(), 2, '.', '');
                    
                    $place['price'] = number_format((float)Review::where('place_id', $place['place_id'])->pluck('price')->avg(), 2, '.', '');
                 
                    $place['reviewNum'] = Review::where('place_id', $place['place_id'])->count();
                    
                    return $place;
                }

            });

            $new = $new[0];

            return response()->json(['success'=>$new], $this-> successStatus, [], JSON_UNESCAPED_SLASHES|JSON_PRETTY_PRINT); 

        } else{
     
            $new = collect($request['placesArray'])->map(function ($place)  {
    
                if(Review::where('place_id', $place['id'])){
    
                    $place['newRating'] = number_format((float)Review::where('place_id', $place['id'])->pluck('rating')->avg(), 2, '.', '');

                    $place['price'] = number_format((float)Review::where('place_id', $place['id'])->pluck('price')->avg(), 2, '.', '');
                   
                    $place['reviewNum'] = Review::where('place_id', $place['id'])->count();
                    
                    return $place;
                }
    
            });
    
            return response()->json(['success'=>$new], $this-> successStatus, [], JSON_UNESCAPED_SLASHES|JSON_PRETTY_PRINT); 
        }

    }




     
    
}
