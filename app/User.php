<?php
namespace App;

use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Review; 
use Carbon;

class User extends Authenticatable
{
  use HasApiTokens, Notifiable, SoftDeletes;
/**
* The attributes that are mass assignable.
  *
  * @var array
  */
  protected $fillable = [
  'name', 'email', 'password','active', 'activation_token', 'radius', 'preference']; //model accepts these params
  /**
  * The attributes that should be hidden for arrays.
  *
  * @var array
  */
  protected $hidden = [
  'password', 'remember_token', 'activation_token'
  ];

  //relationship between review and user
  public function reviews()
  {
    return $this->hasMany(Review::class);
  }

  public function getCreatedAtAttribute($date)
  {
      return Carbon::createFromFormat('Y-m-d H:i:s', $date)->format('Y-m-d');
  }

  public function getUpdatedAtAttribute($date)
  {
      return Carbon::createFromFormat('Y-m-d H:i:s', $date)->format('Y-m-d');
  }

  public function AauthAcessToken(){
    return $this->hasMany('\App\OauthAccessToken');
  }
}