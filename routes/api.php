<?php
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('register', 'UserController@register');

//group for auth middeleware 
Route::group([

    'prefix' => 'auth'

], function () {

    Route::post('login', 'UserController@login');
    Route::get('signup/activate/{token}', 'UserController@signupActivate'); //verifies user

    //gourp for api middleware
    Route::group([

      'middleware' => 'auth:api'

    ], function() {

        Route::post('getReviews', 'ReviewController@getReviews'); //route to retrieve reviews
        Route::post('details', 'UserController@details'); //route to retrieve user details
        Route::post('delete', 'UserController@delete');
        Route::post('update', 'UserController@update');
        Route::post('publish', 'ReviewController@publish');
        Route::post('getAverage', 'ReviewController@getAverage');
        Route::post('setPreferences', 'UserController@setPreferences'); //route to set preferences
        Route::post('logout','UserController@logoutApi'); 
          
    });
});
