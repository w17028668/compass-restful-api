<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReviewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {   
        Schema::create('reviews', function($table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->string('place_id');
            $table->string('review_text');
            $table->string('name');
            $table->double('rating');
            $table->double('price');
            $table->timestamps();
        });
    
       Schema::table('reviews', function($table) {
           $table->foreign('user_id')->references('id')->on('users'); //foreign key 
       });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reviews');
    }
}
